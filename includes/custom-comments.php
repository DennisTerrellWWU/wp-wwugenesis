<?php

/**
* This is a separate file because Genesis lacks the filters we need to customize the comments.
* Instead, we need to replace and customize entire functions, which are located here.
* Changelog:
* 1. Removed link from time/date output on comments
* 2. Moved edit link from by time/date to by reply in entry footer.
* 3. Change the comment form args because Genesis bypasses filter arguments if theme is HTML5. 
* 4. Add 'comment_field' argument to custom_do_comment_form to shrink box size (8-col to 3-col)
*/

function wwugenesis_default_list_comments() {
    $args = array(
        'type'          => 'comment',
        'avatar_size'   => 54,
        'callback'      => 'wwugenesis_comment_callback',
        'max_depth'		=> 5,
    );

    $args = apply_filters( 'genesis_comment_list_args', $args );

    wp_list_comments( $args );
}

/**
 * Comment callback for {@link genesis_default_list_comments()} if HTML5 is active.
 *
 * Does `genesis_before_comment` and `genesis_after_comment` actions.
 *
 * Applies `comment_author_says_text` and `genesis_comment_awaiting_moderation` filters.
 *
 * @since 2.0.0
 *
 * @param stdClass $comment Comment object.
 * @param array    $args    Comment args.
 * @param integer  $depth   Depth of current comment.
 */
function wwugenesis_comment_callback( $comment, array $args, $depth ) {
	$GLOBALS['comment'] = $comment; ?>
	<?php //add a child-comment class to be targeted by jQuery ?>
	<li <?php if ($depth > 1) {comment_class("child-comment");} else {comment_class();} ?> id="comment-<?php comment_ID(); ?>" >
	<article <?php echo genesis_attr( 'comment' ); ?>>

		<?php do_action( 'genesis_before_comment' ); ?>

		<header <?php echo genesis_attr( 'comment-header' ); ?>>
			<p <?php echo genesis_attr( 'comment-author' ); ?>>
				<?php
				echo get_avatar( $comment, $args['avatar_size'] );

				$author = get_comment_author();
				$url    = get_comment_author_url();

				if ( ! empty( $url ) && 'http://' !== $url ) {
					$author = sprintf( '<a href="%s" %s>%s</a>', esc_url( $url ), genesis_attr( 'comment-author-link' ), $author );
				}

				/**
				 * Filter the "comment author says" text.
				 *
				 * Allows developer to filter the "comment author says" text so it can say something different, or nothing at all.
				 *
				 * @since unknown
				 *
				 * @param string $text Comment author says text.
				 */
				$comment_author_says_text = apply_filters( 'comment_author_says_text', __( 'says', 'genesis' ) );

				printf( '<span itemprop="name">%s</span> <span class="says">%s</span>', $author, $comment_author_says_text );
				?>
		 	</p>

			<p <?php echo genesis_attr( 'comment-meta' ); ?>>
				<?php
				printf( '<time %s>', genesis_attr( 'comment-time' ) );
				printf( '<span %s>', genesis_attr( 'comment-time-link' ) );
				echo    esc_html( get_comment_date() ) . ' ' . __( 'at', 'genesis' ) . ' ' . esc_html( get_comment_time() );
				echo    '</span></time>';
				?>
			</p>
		</header>

		<div <?php echo genesis_attr( 'comment-content' ); ?>>
			<?php if ( ! $comment->comment_approved ) : ?>
				<?php
				/**
				 * Filter the "comment awaiting moderation" text.
				 *
				 * Allows developer to filter the "comment awaiting moderation" text so it can say something different, or nothing at all.
				 *
				 * @since unknown
				 *
				 * @param string $text Comment awaiting moderation text.
				 */
				$comment_awaiting_moderation_text = apply_filters( 'genesis_comment_awaiting_moderation', __( 'Your comment is awaiting moderation.', 'genesis' ) );
				?>
				<p class="alert"><?php echo $comment_awaiting_moderation_text; ?></p>
			<?php endif; ?>

			<?php comment_text(); ?>
		</div>

		<?php
			echo '<div class="comment-after">';
			edit_comment_link( __( 'Edit', 'genesis' ));
			comment_reply_link( array_merge( $args, array(
				'depth'  => $depth,
			) ) );
			echo '</div>';
		?>

		<?php do_action( 'genesis_after_comment' ); ?>

	</article>
	<?php
	//* No ending </li> tag because of comment threading
}

/**
 * Optionally show the comment form.
 *
 * Genesis asks WP for the HTML5 version of the comment form - it uses {@link genesis_comment_form_args()} to revert to
 * XHTML form fields when child theme doesn't support HTML5.
 *
 * @since 1.0.0
 *
 * @return null Return early if comments are closed via Genesis for this page or post.
 */
remove_action( 'genesis_comment_form', 'genesis_do_comment_form' );
add_action( 'genesis_comment_form', 'custom_do_comment_form' );
function custom_do_comment_form() {

	//* Bail if comments are closed for this post type
	if ( ( is_page() && ! genesis_get_option( 'comments_pages' ) ) || ( is_single() && ! genesis_get_option( 'comments_posts' ) ) )
		return;
	
	$current_user = wp_get_current_user();
	comment_form( array(
		'format'				=>	'html5',
		'title_reply'			=>	sprintf( __("Posting as " . '<a class="comment-reply-user" href="%1$s">%2$s</a>'),
									  admin_url( 'profile.php'),
									  $current_user->display_name,
									  wp_logout_url( apply_filters( 'the_permalink', get_permalink( )))),

		'comment_notes_after'	=> '',
		'logged_in_as'			=> '',
		'comment_field'			=> $comment_field = '<p class="comment-form-comment">' .
				                   '<textarea id="comment" name="comment" cols="45" rows="3" tabindex="4" placeholder="Add a comment..." aria-required="true"></textarea>' .
				                   '</p>',			
		));

}
