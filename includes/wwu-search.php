<div class="western-search" role="search" aria-label="University and Site Search">
    <button role="button" aria-pressed="false" aria-label="Open the search box"><span class="acc-hide-text">Open Search</span></button>
    <div class="western-search-widget">
        <form id="western-search-widget-form" method="get" action="<?php echo esc_url( home_url() ) ?>">
            <div class="western-search-terms">

             <input type="search" id="searchfield" name="s" size="17" maxlength="50" placeholder="Search" autocomplete="off"/>
            </div>
            <div class="western-search-type-toggle">
                <span class="search-toggle-local">
                    <input type="radio" name="search-toggle" value="search-local" id="search-local" checked>
                    <label for="search-local">This Site</label>
                </span>
                <span class="search-toggle-western">
                    <input type="radio" name="search-toggle" value="search-western" id="search-western">
                    <label for="search-western">All of Western</label>
                </span>
            </div>
            <input type="hidden" name="client" value="default_frontend">
            <input type="hidden" name="output" value="xml_no_dtd">
            <input type="hidden" name="proxystylesheet" value="default_frontend">
            <input type="hidden" name="sort" value="date:D:L:d1">
            <input type="hidden" name="oe" value="UTF-8">
            <input type="hidden" name="ie" value="UTF-8">
            <input type="hidden" name="ud" value="1">
            <input type="hidden" name="exclude_apps" value="1">
            <input type="hidden" name="filter" value="0"/>

            <!-- Change the "sharename" to match your search "Collection" name. Contact web.help@wwu.edu for help. -->
            <input type="hidden" name="site" value="">
        </form> <!--end western-search-widget-form -->
    </div> <!-- end western-search-widget -->
</div>
