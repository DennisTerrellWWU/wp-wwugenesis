<?php
/*
 * WWU Genesis Theme Customizer 
 * Additions:
 * 
 * 1. Add the option for background-size: cover in background images
 */


function wwu_customizer_background_size( $wp_customize ) {
	//Specify settings & their default values
	$default = 'inherit';

	$wp_customize->add_setting('background_image_size', array(
			'default' => $default,
	) );

	//Add the radio button set into the UI
	$wp_customize->add_control('background_image_size', array(
		'label' => 'Background Image Size',
		'section' => 'background_image',
		'settings' => 'background_image_size',
		'type' => 'radio',
		'priority' => 1000,
		'choices' => array(
			'cover' => 'Cover',
			'contain' => 'Contain',
			'inherit' => 'Inherit'
			)
	));
}
add_action( 'customize_register', 'wwu_customizer_background_size');

function customizer_bgsize_output() {
	echo '<style type="text/css">
			body.custom-background {
				background-size:' . get_theme_mod('background_image_size','') . ';
		 </style>';
}
add_action( 'wp_head', 'customizer_bgsize_output', 1000 );

//-----------------------------------------------------------------------------//
