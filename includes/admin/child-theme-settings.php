<?php

/* WWU Genesis admin panel additions
 *
 * This file contains functions that hook into Genesis's admin panel and allow us to add settings of our own.  
 * The first section contains functions that are used to initialize each of our settings areas
 * Every section afterwards is a new settings box that we define.  
 *
 * Relevant definitions: 
 * Sanitation filter - required for all inputs to filter out potentially malicious code.  See https://xkcd.com/327/ for why this is super extra important.
 * Callback - in this context, is always the function that provides the HTML template for the settings box in question.
 *
 * To add a new settings box:
 * 1. Assign it a name and default value in the custom_settings_defaults function.
 * 2. Set up its sanitizer filter in wwu_register_sanitizer_filters based on the type of input it will be receiving (i.e. no_html for a biography field)
 * 3. Register a new meta box with Wordpress like in the wwu_register_display_settings_box function.  
 * 4. Create a callback function to display the HTML.
 * 
 * To add a setting to an existing box:
 * 1. Follow steps 1-2 in the previous section.
 * 2. Add the new setting to the existing callback function (more examples soon)
 *
 * To call the new setting:
 * Use genesis_get_option('setting_name_here')
 *
 * For more help, see this excellent tutorial: http://www.billerickson.net/genesis-theme-options/
 */

//-------------------------FOR ALL CUSTOM SETTINGS------------------------/

/* Define defaults for pre-defined Genesis settings. 
 */
function change_default_genesis_settings($defaults) {
	$defaults['posts_nav'] = 'prev-next';
	$defaults['breadcrumb_posts_page'] = 0;
	$defaults['breadcrumb_front_page'] = 0;
	$defaults['nav_extras'] = '';
	return $defaults;
}
add_filter('genesis_theme_settings_defaults', 'change_default_genesis_settings');


/* Default settings for all custom settings made by WebTech
 */
function custom_settings_defaults($defaults) {
	$defaults['display_post_date'] = 0;
	$defaults['display_post_author'] = 0;
	return $defaults;
}
add_filter('genesis_theme_settings_defaults', 'custom_settings_defaults');


/* Assign a sanitation filter type to each of our custom settings 
 * Genesis provides the filter functions.  
 * Filter options are: 'one_zero', 'no_html', 'safe_html' and 'requires_unfiltered_html'
 */
function wwu_register_sanitizer_filters() {
	genesis_add_option_filter(
		'one_zero',
		GENESIS_SETTINGS_FIELD,
		array( 
				'dispaly_post_date',
				'display_post_author',
			) );
}
add_action('genesis_settings_sanitizer_init', 'wwu_register_sanitizer_filters');


//-------------------------DISPLAY SETTINGS METABOX------------------------/

/* Register Display Settings Metabox
 * Syntax: add_meta_box(id_name, display_title, html_display_function, $_genesis_theme_settings_pagehook, page_context, display_priority)
 * Reference: https://developer.wordpress.org/reference/functions/add_meta_box/
 */
function wwu_register_display_settings_box($_genesis_theme_settings_pagehook) {
	add_meta_box('wwu_display_settings', 'Display', 'wwu_display_settings_box', $_genesis_theme_settings_pagehook, 'main', 'high');
}
add_action('genesis_theme_settings_metaboxes', 'wwu_register_display_settings_box');


/* Callback function to display the HTML for the display settings box.
 * Checked is set to 1 as a filler - the real value is set by the default
 * value in the custom_settings_defaults function
 */
function wwu_display_settings_box() {
	?>
	<p> 
		<label for="<?php echo GENESIS_SETTINGS_FIELD; ?>[display_post_date]">
			<input type="checkbox" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[display_post_date]" value="1" <?php checked(1, genesis_get_option('display_post_date')); ?> />
		<?php echo "Post date?"?></label>
	</p>
	<p>
		<label for="<?php echo GENESIS_SETTINGS_FIELD; ?>[display_post_date]">
			<input type="checkbox" name="<?php echo GENESIS_SETTINGS_FIELD; ?>[display_post_author]" value="1" <?php checked(1, genesis_get_option('display_post_author')); ?> />
		<?php echo "Post author?"?></label>
	</p>
	<?php 
}