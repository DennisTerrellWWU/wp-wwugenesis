<?php 

/**
 * Genesis function override
 * genesis_footer_widget_areas
 * Original located in lib/structure/footer.php
 * ------------------------------------------------------
 * Echo the markup necessary to facilitate the footer widget areas.
 * Check for a numerical parameter given when adding theme support - if none is found, then the function returns early.
 * The child theme must style the widget areas.
 * Applies the `genesis_footer_widget_areas` filter.
 *
 * @since 1.6.0
 * @uses genesis_structural_wrap() Optionally adds wrap with footer-widgets context.
 * @return null Return early if number of widget areas could not be determined, or nothing is added to the first widget area.
 * 
 * Changelog:
 * 1. Prevent the loop from exiting if there was no widget in area 1.  This should let us have a widget in area 2 only.
 */

function western_footer_widget_areas() {

	$footer_widgets = get_theme_support( 'genesis-footer-widgets' );

	if ( ! $footer_widgets || ! isset( $footer_widgets[0] ) || ! is_numeric( $footer_widgets[0] ) )
		return;

	$footer_widgets = (int) $footer_widgets[0];

	$inside  = '';
	$output  = '';
 	$counter = 1;

	while ( $counter <= $footer_widgets ) {
		//* Darn you, WordPress! Gotta output buffer.
		ob_start();
		dynamic_sidebar( 'footer-' . $counter );
		$widgets = ob_get_clean();

		$inside .= sprintf( '<div class="footer-widgets-%d widget-area">%s</div>', $counter, $widgets );

		$counter++;
	}

	if ( $inside ) {
		$output .= genesis_markup( array(
			'html5'   => '<div %s>',
			'xhtml'   => '<div id="footer-widgets" class="footer-widgets">',
			'context' => 'footer-widgets',
		) );
	
		$output .= genesis_structural_wrap( 'footer-widgets', 'open', 0 );
		$output .= $inside;
		$output .= genesis_structural_wrap( 'footer-widgets', 'close', 0 );
		$output .= '</div>';

	}

	echo apply_filters( 'genesis_footer_widget_areas', $output, $footer_widgets );

}