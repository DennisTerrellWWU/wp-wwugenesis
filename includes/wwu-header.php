<nav role="navigation" class="western-nav" aria-label="University Links, Search, and Navigation">
    <a class="mobile-logo" href="//www.wwu.edu"></a>
    <?php 
        include('quick-links.php');
        
        global $detect;
        if ( !$detect -> isMobile() ) {
            include('wwu-search.php'); 
        }
    ?>
    <button class="mobile-nav-button" role="button" aria-pressed="false" aria-label="Open Mobile Main Navigation">
        <div class="button-wrap">
            <span>Menu</span>
            <div class="nav-icon"></div>
        </div>
    </button>
</nav>

