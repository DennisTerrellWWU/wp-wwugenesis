<div class="western-quick-links" aria-label="Western Quick Links">
      <ul>
        <li><a href="//www.wwu.edu/academic_calendar" title="Calendar">
        	<span>Calendar</span>
        </a></li>
        <li><a href="//www.wwu.edu/directory" title="Directory">
        	<span>Directory</span>
        </a></li>
        <li><a href="//www.wwu.edu/index" title="Index">
        	<span>Index</span>
        </a></li>
        <li><a href="//www.wwu.edu/campusmaps" title="Map">
        	 <span>Map</span>
        </a></li>
        <li><a href="//mywestern.wwu.edu" title="myWestern">
        	<span>myWestern</span>
        </a></li>
      </ul>
</div>