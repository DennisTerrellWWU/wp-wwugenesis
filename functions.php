<?php

//*Start Genesis
include_once( get_template_directory() . '/lib/init.php' );

//*Include our admin panel modifications & additional settings
include_once( 'includes/admin/child-theme-settings.php');
include_once( 'includes/admin/customizer.php');

//*Constants & Child Theme Properties
define( 'CHILD_THEME_NAME', 'WWU Genesis' );
define( 'CHILD_THEME_URL', '//www.wwu.edu/webtech' );
define( 'CHILD_THEME_VERSION', '0.7' );
define( 'THEME_PATH', get_stylesheet_directory_uri());
define( 'THEME_PATH_REL', get_stylesheet_directory());



//Detect browser type server side on load
require_once 'includes/Mobile_Detect.php';
$detect = new Mobile_Detect;
define ('IS_MOBILE', $detect -> isMobile());

//Add body class if UA is mobile
add_filter( 'body_class', 'add_body_class');
function add_body_class($classes) {
	IS_MOBILE ? $classes[]='is-mobile' : $classes[]='not-mobile';
	return $classes;
}

//---------------------  GENESIS SETTINGS ---------------------//
//*Remove unsupported default Genesis layouts
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

//--------------------- SCRIPTS ---------------------//
/* Stop wordpress from including local jQuery scripts.  Import from CDN instead.
Import other scripts into footer. */
add_action( 'wp_enqueue_scripts', 'wwu_include_scripts');
function wwu_include_scripts() {
	wp_deregister_script('jquery');
	wp_register_script('jquery',  'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js', '1.8.2');
	wp_register_script( 'fastclick', 'https://cdnjs.cloudflare.com/ajax/libs/fastclick/1.0.6/fastclick.min.js', array('jquery'), '1.0.6', true);
	wp_register_script( 'western', THEME_PATH . '/js/western.js', array('jquery'), '1.0', true);
	wp_register_script( 'velocity', 'https://cdnjs.cloudflare.com/ajax/libs/velocity/1.2.2/velocity.min.js', array(), '1.2.2', true);

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'western' );
	wp_enqueue_script( 'fastclick' );
	wp_enqueue_script( 'velocity' );
}

//--------------------- HEADER --------------------- //
//* Add Western header and search
add_action( 'genesis_before_header', 'western_header' );
function western_header() {
	include ('includes/wwu-header.php');
}

/* Create custom CSS for the banner image (i.e. only dynamically generate image
 * url - allow the rest to be set in the stylesheet. Heavily based on
 * Genesis' function in lib/structure/header.php
 */

add_action( 'wp_head', 'western_banner_style');

function western_banner_style() {

	$output = '';
	$header_image = get_header_image();
	$text_color   = get_header_textcolor();

	// If no options set, don't waste the output. Do nothing.
	if ( empty( $header_image ) && ! display_header_text() && $text_color === get_theme_support( 'custom-header', 'default-text-color' ) )
		return;

	$header_selector = get_theme_support( 'custom-header', 'header-selector' );
	$title_selector  = genesis_html5() ? '.custom-header .site-title'       : '.custom-header #title';
	$desc_selector   = genesis_html5() ? '.custom-header .site-description' : '.custom-header #description';

	// Header selector fallback
	if ( ! $header_selector )
		$header_selector = genesis_html5() ? '.custom-header .site-header' : '.custom-header #header';

	// Header image CSS, if exists
	if ( $header_image )
		$output .= sprintf( '%s { background-image: url(%s); }', $header_selector, esc_url( $header_image ) );

	// Header text color CSS, if showing text
	if ( display_header_text() && $text_color !== get_theme_support( 'custom-header', 'default-text-color' ) )
		$output .= sprintf( '%2$s a, %2$s a:hover, %3$s { color: #%1$s !important; }', esc_html( $text_color ), esc_html( $title_selector ), esc_html( $desc_selector ) );

	if ( $output )
		printf( '<style type="text/css">%s</style>' . "\n", $output );
}

//* Insert wwu favicon
add_filter('genesis_pre_load_favicon', 'add_wwu_favicon');
function add_wwu_favicon( $favicon_url ) {
	return THEME_PATH . '/img/favicon.ico';
}

//*Remove header widget area
unregister_sidebar( 'header-right' );

//* Custom headers arguments
$headerArgs = array(
	'default-image' => THEME_PATH . '/img/headers/mountains.jpg',
	'flex-width'	=> true,
	'flex-height' 	=> true,
	'width'			=> 1200,
	'height' 		=> 250,
	'uploads' 		=> true,
	'header-text'	=> true,
	'wp-head-callback' => 'western_banner_style', //prevents Genesis from creating default style element
);
add_theme_support( 'custom-header', $headerArgs);

    
//* Adds all images in /img/headers/ to list of default headers
if (!function_exists('add_suggested_headers')){
    function add_suggested_headers() {
        $arrayOfHeaders = array();
        $headers = array(
            "arial" => 'arial.jpg',
            "bike" => 'bike.jpg',
            "clouds" => 'clouds.jpg',
            "fireworks" => 'fireworks.jpg',
            "fog" => 'fog.jpg',
            "lawn" => 'lawn.jpg',
            "miller-hall" => 'miller-hall.jpg',
            "moon" => 'moon.jpg',
            "mountains" => 'mountains.jpg',
            "old-main" => 'old-main.jpg',
            "park" => 'park.jpg',
            "soccer-field" => 'soccer-field.jpg',
            "trees" => 'trees.jpg',
            "vu-sunset" => 'vu-sunset.jpg',
        );
        $url = content_url();
        $i = 0;
        foreach($headers as $header){
            if ($header[0] != '.'){
                $arrayOfHeaders[$i] = array(
                    'url' => $url . '/themes/wp-wwugenesis/img/headers/' . $header,
                    'thumbnail_url' => $url . '/themes/wp-wwugenesis/img/headers/' . $header,
                    'description' => __($i, 'wwu_genesis')   
                );
            };
        
        $i++;
        };
        
        register_default_headers( $arrayOfHeaders );
    };

    add_suggested_headers();
};


//*Edit breadcrumb arguments
add_filter( 'genesis_breadcrumb_args', 'custom_breadcrumb_args' );
function custom_breadcrumb_args( $args ) {
	$args['sep'] = ' > ';
	return $args;
}

//*Insert HTML for logo.  Priority 3 to insert above title.
add_action( 'genesis_header', 'add_logo', 5 );
function add_logo() {
	echo '<a href="//www.wwu.edu" class="western-logo"></a>';
}

//*Add post/page title to header for mobile (replaces site description)
add_action( 'genesis_site_description', 'add_mobile_title');
function add_mobile_title() {

	if( is_category() ) {
		echo '<div class="page-title cat">', single_cat_title('Category: ', false),  '</div>';
	}
	else if( is_tag() ) {
		echo '<div class="page-title tag">', single_cat_title('Posts tagged with: ', false),  '</div>';
	}
	else if ( is_author() ) {
		echo '<div class="page-title author">', 'Posts for author ', get_the_author(), '</div>';
	}
	else if ( is_archive() ) {
		the_archive_title('<div class="page-title archive">', '</div>');
	}
	else if (is_search() ) {
		echo '<div class="page-title search">', 'Search', '</div>';
	}
	else if (('page' == get_option('show_on_front') && is_home())) {
		$blogTitle = get_the_title( get_option('page_for_posts') );
		echo '<div class="page-title blog">', $blogTitle, '</div>';
	}
	else {
		the_title('<div class="page-title">', '</div>');
	}
}

//*Wrap header & nav in div
add_action( 'genesis_before_header', 'wrap_start');
function wrap_start() {
	echo '<div class="header-container custom-header">';
}
add_action( 'genesis_after_header', 'wrap_end');
function wrap_end() {
	echo '</div>';
}


//---------------------  NAVIGATION  ---------------------//
//*Reposition Nav to before header by default.  For mobile by default.  CSS positions on desktop.
remove_action( 'genesis_after_header', 'genesis_do_nav');
add_action( 'genesis_before_header', 'genesis_do_nav');

//*Remove secondary nav area.  Should be primary or a widget.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'Primary Navigation Menu', 'genesis' ) ) );

//*Customize args that Genesis passes to the WordPress nav class:
//*Limit the depth of nav menus to 3 (1 top level, 2 sub)
add_filter('wp_nav_menu_args', 'wwu_primary_menu_args');
function wwu_primary_menu_args($args) {
	if( 'primary' == $args['theme_location'] ) {
	    $args['depth'] = 3;
	    $args['link_before']='<span class="sub-menu-expand"></span>';
  	}
  	return $args;
}

//Inject search into nav if the device is mobile
//ob_start parses include file into string
add_filter('wp_nav_menu', 'do_mobile_search', 10, 2);
function do_mobile_search($nav_menu, $args) {
	$isGenesisPrimary = strpos($args->menu_class, 'genesis-nav-menu');
	global $detect;
	if($isGenesisPrimary && $detect -> isMobile()) {
		ob_start();
		include 'includes/wwu-search.php';
		$search = ob_get_clean();
		return $search . $nav_menu;
	}
	else {
		return $nav_menu;
	}
}

//--------------------- POST CONTENT --------------------- //
//*Wrap each blog post title & article in div
add_action('genesis_entry_header', 'post_wrap_start', 1);
function post_wrap_start() {
	echo '<div class="post_container">';
}
add_action('genesis_entry_footer', 'post_wrap_end', 1);
function post_wrap_end() {
	echo '</div>';
}

//*Edit post meta output to include author and remove text
add_filter( 'genesis_post_meta', 'sp_post_meta_filter' );
function sp_post_meta_filter($post_meta) {
	if ( !is_page() ) {
		$post_meta = '';
		if(genesis_get_option('display_post_author')) {
			$post_meta = '[post_author_posts_link]';
		}
		$post_meta .= '[post_categories before=""] [post_tags before=""]';
		return $post_meta;
	}
}


//Remove the post edit link from page & post content
add_filter('genesis_edit_post_link', 'wwu_enable_post_link');
function wwu_enable_post_link() {
	return false;
}
//Add back into the post title
add_filter('genesis_post_title_output', 'wwu_move_edit_link');
function wwu_move_edit_link($title) {
	$title .= edit_post_link( __( '' ), '', '' );
	return $title;
}

/* Filter out post author from top of post
 * Optionally, display date based on settings */
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter($post_info) {
	if ( !is_page() ) {
		$post_info = '';
		if( genesis_get_option('display_post_date')) {
			$post_info = '[post_date]';
		}
		return $post_info;
	}
}

//*Edit the cutoff text for post excerpts.  Default is [...]
add_filter( 'excerpt_more', 'wwu_excerpt_more' );
function wwu_excerpt_more( $more ) {
	return '...';
}

//Remove default gallery styles because they always override our css
add_filter( 'use_default_gallery_style', '__return_false' );


//---------------- USER-FACING EDITOR --------------- //
add_filter( 'mce_buttons_2', 'add_editor_buttons');
function add_editor_buttons($buttons) {
	$buttons[] = 'superscript';
	$buttons[] = 'subscript';
	$buttons[] = 'styleselect';
	$buttons[] = 'code';
	return $buttons;
}

//--------------------- FOOTER  ---------------------//
//Inject WWU card code into footer
$card = <<<EOD
	<div class="card-container">
		<div class="western-card">
			<h1><a href="//www.wwu.edu">Western Washington University</a></h1>

			<div class="western-contact-info">
				<p class="western-address">516 High Street<br>
				Bellingham, WA 98225</p>
				<p class="western-telephone"><a href="tel:3606503000">(360) 650-3000</a></p>
				<p class="western-contact"><a href="//www.wwu.edu/wwucontact/">Contact Western</a></p>
			</div>

			<div class="western-social-media">
				<ul>
					<li class="western-facebook-icon"><a href="//www.facebook.com/westernwashingtonuniversity">Facebook</a></li>
					<li class="western-flickr-icon"><a href="//www.flickr.com/wwu">Flickr</a></li>
					<li class="western-twitter-icon"><a href="//twitter.com/#!/WWU">Twitter</a></li>
					<li class="western-youtube-icon"><a href="//www.youtube.com/wwu">Youtube</a></li>
					<li class="western-googleplus-icon"><a href="//plus.google.com/+wwu/posts">Google Plus</a></li>
					<li class="western-rss-icon"><a href="//news.wwu.edu/go/feed/1538/ru/atom/">RSS</a></li>
				</ul>
			</div>
		</div>
	</div>
EOD;

//Print western card
function wwu_footer() {
	global $card;
	echo $card;
}
//Add footer override functions into theme
include ('includes/custom-footer.php');

//*Reposition Footer to outsite site-container for full-width
//*Also move widgets from before footer (default for some reason) to inside the footer.  11,12,13 order is extremely important.
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );
add_action( 'genesis_after', 'genesis_footer_markup_open', 11 );
add_action( 'genesis_after', 'western_footer_widget_areas', 12);
add_action( 'genesis_after', 'wwu_footer', 12);
add_action( 'genesis_after', 'genesis_footer_markup_close', 13 );

//* Add support for 2-column footer widgets
add_theme_support( 'genesis-footer-widgets', 2 );

//--------------------- WIDGETS ---------------------//


//--------------------- COMMENTS ---------------------//
//Customized in separate include file (includes/custom-comments.php) because of function length
//List of args: https://codex.wordpress.org/Function_Reference/wp_list_comments
remove_action( 'genesis_list_comments', 'genesis_default_list_comments');
add_action( 'genesis_list_comments', 'wwugenesis_default_list_comments' );
include ('includes/custom-comments.php');

//Remove "author says" text before username
add_filter( 'comment_author_says_text', 'comment_author_says_text' );
function comment_author_says_text() {
	return '';
}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom background
add_theme_support( 'custom-background' );

//Featured image preview support
add_theme_support( 'post-thumbnails');



