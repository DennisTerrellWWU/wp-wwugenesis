#WP-WWUGENESIS#
The official Western Washington University (WWU) WP theme.

##Dependencies##
We are using [bundler](https://github.com/at-import/breakpoint/wiki/Installation)
 to manage our SASS dependencies. If you don't use bundler,
please [install it](http://bundler.io/#getting-started).

To compile your SASS from the root directory of the project run `bundle exec compass compile`.

##External Libraries##
---------------------

1. Mobile Detect PHP (http://mobiledetect.net/)
2. Fastclick JS (https://github.com/ftlabs/fastclick)
3. Velocity JS (http://julian.com/research/velocity/)