<?php
/**
 * Custom 404 page for WWU Genesis.
 */

//* Remove default loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_loop', 'genesis_404' );
/**
 * This function outputs a 404 "Not Found" error message
 */
function genesis_404() {

	echo genesis_html5() ? '<article class="entry page">' : '<div class="post hentry">';

		printf( '<h1 class="entry-title">%s</h1>', __( 'Page not found, error 404', 'genesis' ) );
		echo '<div class="entry-content">';

			if ( genesis_html5() ) :

				echo '<p>' . sprintf( __( 'The page you are looking for does not exist. You can return to the <a href="%s">homepage</a> or use the search form below to find what you are looking for.', 'genesis' ), home_url() ) . '</p>';

				echo '<p>' . get_search_form() . '</p>';

			else :
	?>

			<p><?php printf( __( 'The page you are looking for does not exist. You can return to the <a href="%s">homepage</a> to find what you are looking for.', 'genesis' ), home_url() ); ?></p>

<?php
			endif;

			echo '</div>';

		echo genesis_html5() ? '</article>' : '</div>';

}

genesis();