/**
 * Custom JS for the WWU-Genesis theme
 */

//------------------------ ON LOAD -------------------------//
//Attach Fastclick
$(function () {
  FastClick.attach(document.body);
});

//If toggle button is hidden (desktop view), menu should be visible
$(window).on('resize', function (){
  if (mainNavButton.css("display") == "none" & mainNav.css("display") == "none") {
    mainNav.slideToggle();
  }
});

//Browser-based detection if modernizer doesn't work with test feature
var browser = $.browser;
var isMobile = $("body").hasClass("is-mobile");

//--------------------- MOBILE NAVIGATION ---------------------//
var mainNavButton = $(".mobile-nav-button");
var mainNav = $(".nav-primary");
var subMenuParent = $('.nav-primary li.menu-item-has-children');
var subMenuLink = $('.nav-primary li.menu-item-has-children a');

//Nav Button click to toggle display
mainNavButton.toggle(function () {
  if(isMobile && $(window).width() > 770) {
    mainNav.velocity("fadeIn", 190);
  }
  else {
    mainNav.css("opacity", "1");
    mainNav.velocity("slideDown", "easeOutCubic");
  }

}, function () {
  if(isMobile && $(window).width() > 770) {
    mainNav.velocity("fadeOut", 190);
  }
  else {
    mainNav.velocity("slideUp", "easeOutQuad");
  }
});

subMenuParent.click(function (event) {
  var $this = $(this);

  event.stopPropagation();

  $this.toggleClass("expanded");

  if ($this.hasClass("expanded")) {
    $this.children('ul').show();
  } else {
    $this.children('ul').hide();
  }
});

subMenuLink.click(function (event) {
  event.stopPropagation();
});

//--------------------- SHOW HIDE COMMENTS --------------------- //
var comments = $('.comment-list');
var commentsCount = $('.comment').length;
var toggleCommentsButton = $('<input/>')
  .attr('type', 'submit')
  .attr('id', 'toggle-comments-button')
  .attr('href', '#')
  .attr('value', 'Show comments - ' + commentsCount + ' comments')
  .click(toggleComments)
  .insertBefore(comments);

// Comments are initially hidden.
comments.hide();

function toggleComments(event) {
  comments.toggle();
  toggleCommentsButton.val(function (index, value) {
    return (value.indexOf('Show') >= 0) ? value.replace('Show', 'Hide') : value.replace('Hide', 'Show');
  });
}

//--------------------- WESTERN SEARCH ---------------------//
var searchButton = $(".western-search > button");
var searchWidget = $(".western-search-widget");
var quickLinks = $(".western-quick-links");

function openSearch() {
  searchWidget.velocity({top: 0}, "easeOutQuint");
  searchButton.addClass("is-open");
  $(".western-search-terms > input:nth-child(1)").focus();
  browser.msie ? quickLinks.hide() : quickLinks.velocity({blur: 3}, 'easeOutCubic');
}

function closeSearch() {
  $('.western-search-type-toggle').hide();
  searchWidget.velocity("reverse");
  searchButton.removeClass("is-open");
  browser.msie ? quickLinks.show() : quickLinks.velocity("reverse", 0.1);
}

/* select the search box and animate open */
searchButton.toggle(openSearch, closeSearch);

// Toggle between local search on wordpress and Western Google Search Appliance
var searchForm = $("#western-search-widget-form");
var localSearchString = searchForm.attr( 'action' );
var searchInputField = $("#searchfield");
var searchToggle = $("input:radio[name=search-toggle]");

//Set to Wordpress search settings initially
searchInputField.attr('name', 's');
$(".search-toggle-local").addClass("selected");
searchInputField.attr('placeholder', 'Search this site...');

//Display site target toggle on user input
searchInputField.on("change keydown paste click", function(event) {
  var text = searchInputField.val();
  var code = event.keyCode || event.which;
  if(code == 27) {
    if(text !== "") {
      searchInputField.value = "";
    }
    else {
      closeSearch();
    }
  }
  else {
    $('.western-search-type-toggle').show();
  }
});

//Hide Search toggle when closed on desktop
searchWidget.blur(function () {
  $('.western-search-type-toggle').hide();
});

//Change search target & toggle class on radio button change
searchToggle.change(function () {
  if($(this).val() == 'search-local') {
    searchInputField.attr('name', 's');
    searchForm.attr('action', localSearchString);
    $('.search-toggle-local').addClass("selected");
    $('.search-toggle-western').removeClass("selected");
    searchInputField.attr('placeholder', 'Search this site...');
  }
  else if($(this).val() == 'search-western') {
    //for some reason, google search appliance needs "q" to be the name of the input.
    searchInputField.attr('name', 'q');
    searchForm.attr('action', 'http://search.wwu.edu/search');
    $('.search-toggle-local').removeClass("selected");
    $('.search-toggle-western').addClass("selected");
    searchInputField.attr('placeholder', 'Search all WWU sites...');
  }
});

//--------------------- SHOW/HIDE CONVERSATION COMMENTS ---------------------//
var parentComments = $('.comment > .children').parent();
var topLevelComments = parentComments.filter('.depth-1');
var commentAfter = parentComments.children('article').children('.comment-after');
var expand = $('<a/>')
  .attr('class', 'expand-comments-link')
  .attr('href', '#')
  .click(onClickHide)
  .prependTo(commentAfter)
  .click();

function onClickShow(event) {
  event.preventDefault();
  showChildrenComments(event.target);
}

function onClickHide(event) {
  event.preventDefault();
  hideChildrenComments(event.target);
}

function showChildrenComments(element) {
  $(element).off();
  $(element).click(onClickHide);
  $(element).closest('.comment').children('.children').slideDown();
  $(element).text('Collapse');
}

function hideChildrenComments(element) {
  $(element).off();
  $(element).click(onClickShow);
  $(element).closest('.comment').children('.children').slideUp();
  $(element).text('Expand');
}

//-------------------------- RESPONSIVE TABLES ------------------------------//

$(document).ready(function() {
	//Add responsive class to all tables automatically
	$("table:not(#wp-calendar)").addClass("responsive");
	
	//Make responsive<->regular when window is resized		
	var switched = false;
	var updateTables=function() {
		if(($(window).width() <= 770) && !switched) {
			switched = true;
			$("table.responsive").each(function(i,element) { 
				splitTable($(element));
			});
			return true;
		} else if(switched&&($(window).width() > 770)) {
			switched = false;
			$("table.responsive").each(function(i,element) {
				unsplitTable($(element));
			});
		}
	};
	$(window).load(updateTables);
	$(window).bind("resize",updateTables);
	
	function splitTable(original) {
		original.wrap("<div class='table-wrapper' />");
		var copy=original.clone();
		copy.find("td:not(:first-child), th:not(:first-child)").css("display","none");
		copy.removeClass("responsive");
		original.closest(".table-wrapper").append(copy);
		copy.wrap("<div class='pinned' />");
		original.wrap("<div class='scrollable' />");
	}
	
	function unsplitTable(original) {
		original.closest(".table-wrapper").find(".pinned").remove();
		original.unwrap();
		original.unwrap();
	}
});
